#include <iostream>
#ifndef Demo_h
#define Demo_h
using namespace std;


class Investment{
    private:  //properties
         long double value;
         float interest_rate; 
         


    public:  //methods
          Investment();//default constructor 

          void setVal(long double val);

          long double getVal();

          void setInterest(float Intrate);

          float getInterest();

          void calculate();

 };
 #endif

