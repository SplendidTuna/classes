#include "Demo.h"
#include <iostream>
using namespace std;



 Investment::Investment(){
   value = 100;
   interest_rate = 8.3;
 }

 void Investment::setVal(long double val){
   value = val;
 }

 long double Investment::getVal(){
  return value;
 }

 void Investment::setInterest(float Intrate){
   interest_rate = Intrate;
 }
  
 float Investment::getInterest(){
   return interest_rate;
 }

 void Investment::calculate(){
   int months = 1;
   while (value <= 1000000){
      value += 250.00;
      if ((months >= 12) && (months % 12 == 0)){
         int temp = (interest_rate / 100) * value;
         value += temp;
       }
      months++;
  }

  
  cout << value << endl;
  cout << months;
 }